import os
import shlex
import ssl
import subprocess
import logging

from collections import defaultdict
from html.parser import HTMLParser
from http.cookiejar import MozillaCookieJar
from typing import Tuple, Optional

import requests
import urllib3

from config import jenkins_username

kerberos_username = jenkins_username + '@CERN.CH'

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


SLOG = logging.getLogger(__name__)

class LinkParser(HTMLParser):
    def error(self, message):
        pass

    def __init__(self):
        HTMLParser.__init__(self)
        self.kerberos_url = ""

    def handle_starttag(self, tag, attrs):
        attrs_d = defaultdict(default_factory=lambda: "")
        attrs_d.update(dict(attrs))
        if tag == "a":
            id_ = attrs_d.get('id')
            if id_ == 'social-kerberos':
                self.kerberos_url = attrs_d['href']


VERSION = '0.6'

CERN_SSO_CURL_USER_AGENT_KRB = 'curl-sso-kerberos/' + VERSION + ' (Mozilla)'
CERN_SSO_CURL_ADFS_EP = '/auth/realms'
CERN_SSO_CURL_CAPATH = '/etc/ssl/certs'


def get_session(url, cert=None, key=None, use_cert=False) -> Tuple[Optional[requests.Session], Optional[str]]:
    SLOG.info("Getting session")
    if use_cert:
        raise RuntimeError("Certificate is not supported")

    SLOG.info("Creating kerberos token")
    subprocess.check_call(shlex.split(f'kinit  -kt bot.keytab {jenkins_username}@CERN.CH'), timeout=15)
    cookieFileName = "./cookies.txt"
    if os.path.exists(cookieFileName):
        SLOG.info(f"Removing {cookieFileName}")
        os.remove(cookieFileName)
    SLOG.info("Getting Auth Cookie...")
    subprocess.check_call(shlex.split(f'auth-get-sso-cookie --url https://lcgapp-services.cern.ch/spi-jenkins -o {cookieFileName}'), timeout=15)
    SLOG.info("...done")

    SLOG.info("Creating session object now")
    s = requests.Session()
    cookies = MozillaCookieJar("cookies.txt")
    cookies.load()
    s.cookies = cookies

    r = s.get(url, allow_redirects=True, verify=False)
    SLOG.info(f"Response from {url}: {r}")
    if r.status_code != requests.codes.ok:
        SLOG.warning("Returning for bad status code!")
        return None, "Bad status code from get {1}: {0}".format(r.status_code, url)  # + r.text

    if CERN_SSO_CURL_ADFS_EP not in r.url:
        return s, "SSO not detected"

    parser = LinkParser()
    parser.feed(r.text)
    if not parser.kerberos_url:
        return None, "Kerberos login URL not found"

    r = s.get('https://auth.cern.ch' + parser.kerberos_url)
    if r.status_code != requests.codes.ok:
#        with open('resp.html', 'w') as f:
#            print(r.text, file=f)
        return None, "Bad status code from kerberos {1}: {0}".format(r.status_code, parser.kerberos_url)

    if CERN_SSO_CURL_ADFS_EP in r.url:
#        with open('resp.html', 'w') as f:
#            print(r.text, file=f)
        return None, "Bad redirect after post: {0}".format(r.url)

    return s, r.status_code


if __name__ == "__main__":
    sess, err = get_session("https://lcgapp-services.cern.ch/spi-jenkins/", use_cert=False)
    if sess is not None:
        resp = sess.get("https://lcgapp-services.cern.ch/spi-jenkins/")
        resp.raise_for_status()
        with open('resp.html', 'w') as f:
            print(resp.text, file=f)
        print(resp)
    else:
        print("Error:", err)
