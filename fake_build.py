# noinspection PyMethodMayBeStatic
class DryRunBuild(object):
    def __init__(self, params):
        self.params = params

    @property
    def name(self):
        return 'Testbuild #0 {LCG_VERSION}-{LABEL}-{COMPILER}-{BUILDTYPE}'.format(**self.params)

    def get_number(self):
        return 0

    @property
    def baseurl(self):
        return '/'

    def get_status(self):
        return 'DRYRUN'

    def poll(self):
        return
