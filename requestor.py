"""
Jenkins API does not use persistent HTTP connections feature of request Python module

This class extends jenkinsapi.utils.requester.Requester class and uses requests.Session()
"""

import logging
from requests import ConnectionError

from jenkinsapi.utils.requester import Requester
from jenkinsapi.custom_exceptions import JenkinsAPIException

import cernsso

from pprint import pprint

class SSORequester(Requester):
    """
    Wrap get_url, post_url, post_and_confirm_status  methods that make HTTP requests and use HTTP connections pooling.

    If we fail any call we try re-creating our authentication cookies and trying again
    """
    _request = None
    baseurl = None

    def __init__(self, username=None, password=None, ssl_verify=True, baseurl=None):
        super(SSORequester, self).__init__(username, password, ssl_verify, None, baseurl)
        self.baseurl = baseurl
        self.logger = logging.getLogger(__name__)
        self.recreate_session()

    @property
    def request(self):
        if self._request is None:
            s, err = cernsso.get_session(self.baseurl, use_cert=False)
            if s is not None:
                self._request = s
            else:
                raise RuntimeError("SSO session failed: {0}".format(err))
        return self._request

    def get_url(self, url, params=None, headers=None, allow_redirects=True, stream=False, resubmitted=False):
        try:
            resp = super().get_url(url=url, params=params, headers=headers, allow_redirects=allow_redirects)
        except ConnectionError:  # we just retry until we no longer get connectionError?
            return self.get_url(url, params, headers, allow_redirects, resubmitted)
        self.logger.debug(f"get_url <{url}>: {resp} (redirect: {resp.headers.get('location', '(none)')}), final URL {resp.url}")
        if 'auth/realms' in resp.url:
            if resubmitted:
                raise RuntimeError("Recreating session failed, quitting!")
            self.recreate_session()
            return self.get_url(url=url, params=params, headers=headers, allow_redirects=allow_redirects, stream=stream, resubmitted=True)
        return resp

    def post_url(self, url, params=None, data=None, files=None, headers=None, allow_redirects=True, resubmitted=False, **kwargs):
        resp = super().post_url(url=url, params=params, data=data, files=files, headers=headers, allow_redirects=allow_redirects, **kwargs)
        self.logger.debug('post_url <{}>: {} (redirect: {})'.format(url, resp, resp.headers.get('location', '(none)')))
        if 'auth/realms' in resp.url or 'auth/realms' in resp.headers.get('location', '(none)'):
            if resubmitted:
                raise RuntimeError("Recreating session failed, quitting!")
            self.recreate_session()
            return self.post_url(url=url, params=params, data=data, files=files, headers=headers, allow_redirects=allow_redirects, resubmitted=True, **kwargs)

        return resp

    def post_and_confirm_status(self, url, data, params, files, valid, allow_redirects, resubmitted=False):
        try:
            resp = super().post_and_confirm_status(url=url, data=data, params=params, files=files, valid=valid, allow_redirects=allow_redirects)
            self.logger.info('pac_url <{}>: {} (redirect: {})'.format(url, resp, resp.headers.get('location', '(none)')))
        except JenkinsAPIException as e:
            self.logger.error(f"Failed posting and confirm: {e!r}")
            if resubmitted or "status=401" not in str(e):
                raise
            self.recreate_session()
            return super().post_and_confirm_status(url=url, data=data, params=params, files=files, valid=valid, allow_redirects=allow_redirects)
        if 'auth/realms' in resp.url or 'auth/realms' in resp.headers.get('location', '(none)'):
            if resubmitted:
                raise RuntimeError("Recreating session failed, quitting!")
            self.recreate_session()
            return super().post_and_confirm_status(url=url, data=data, params=params, files=files, valid=valid, allow_redirects=allow_redirects)
        return resp

    def recreate_session(self):
        if self._request:
            self._request.close()
            self._request = None
        self.logger.warning("SSO redirect encountered, recreating session")
        s, err = cernsso.get_session(self.baseurl, use_cert=False)
        if s is not None:
            self._request = s
            # jenkinsapi uses session in its functions
            self.session = s
