#!/bin/bash -e
tag=${1:=latest}
docker login gitlab-registry.cern.ch
docker build --network host -t gitlab-registry.cern.ch/sft/lcgbot:$tag .
docker push gitlab-registry.cern.ch/sft/lcgbot:$tag
