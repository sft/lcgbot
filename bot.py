#!/usr/bin/env python3
import asyncio
import copy
import itertools
import logging
import os
import re
import sys
from html import escape
from typing import Iterable, Union, Optional
from pprint import pformat

import expression
from aiohttp import ClientSession
from gidgetlab.aiohttp import GitLabBot, GitLabAPI
from gidgetlab.sansio import Event
from jenkinsapi.build import Build
from jenkinsapi.jenkins import Jenkins
from jenkinsapi.queue import QueueItem
from six import string_types

from MatrixJob import MatrixJob
from config import jenkins_username, jenkins_password, gitlab_token, gitlab_secret
from fake_build import DryRunBuild
from requestor import SSORequester

# logging.getLogger().setLevel(logging.DEBUG)

os.environ.setdefault('GL_ACCESS_TOKEN', gitlab_token)
os.environ.setdefault('GL_SECRET', gitlab_secret)

bot = GitLabBot("lcgbot", url="https://gitlab.cern.ch/")
r: Optional[SSORequester] = None
j: Optional[Jenkins] = None
test_job: MatrixJob
latest_job: MatrixJob
logger: logging.Logger

emoji_status = {'FAILURE': u'📛', 'ABORTED': u'☠️', 'UNSTABLE': u'⚠', 'ERROR': u'🚫', 'SUCCESS': u'✅', 'DRYRUN': u'🚧'}
rex_match = re.compile(r"([A-Z_]+)\s*=~\s*('.*?')")

SUPPORTED_TOOLCHAINS = ['dev3',
                        'dev4',
                        'dev4cuda',
                        'dev3cuda',
                        'dev3python312',
                        'devswan',
                        'devnxcals',
                        'dev3lhcb',
                        'dev4lhcb',
                        'devAdePT',
                        'devgeantv',
                        'devkey',
                        'devkey-head',
                        ]

CAN_BUILD = ['sailer', 'dkonst', 'igoulas', 'sftnight', 'gganis', 'mato', 'gastewart', 'jiling', 'lcgbot',
             'project_741_bot_c499f1eb60c0f643bbeacde0fe27333b',
             'nlanggut',
             "jcarcell",
             'goulas',
             ]

bot_debug = False
bot_dryrun = False


def botsay(text):
    return u'🤖💬 ' + escape(text)


def setup_logging(logfile, debug):
    global logger
    logging.basicConfig(format="%(asctime)s.%(msecs)03d %(name)-10s %(levelname)-8s %(message)s",
                        level=logging.INFO,
                        stream=sys.stdout,
                        datefmt="%d-%m-%Y %I:%M:%S",
                        )
    logger = logging.getLogger("Bot")
    logger.propagate = True
    file_handler = logging.FileHandler(logfile, "w")
    logger.addHandler(file_handler)

    if not debug:
        logger.setLevel(logging.INFO)
    else:
        logger.info("Debug logging is ON")
        logger.setLevel(logging.DEBUG)

    logging.getLogger("urllib3").setLevel(logging.WARNING)


async def post_comment(body, project_id, mr_id):
    if body.startswith('#'):
        body = '\n' + body
    url2 = f"/projects/{project_id}/merge_requests/{mr_id}/notes"
    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')
        res = await gl.post(url2, data={'body': botsay(body)})
        return res


async def update_comment(body, project_id, mr_id, note_id):
    if body.startswith('#'):
        body = '\n' + body
    url2 = f"/projects/{project_id}/merge_requests/{mr_id}/notes/{note_id}"
    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')
        res = await gl.put(url2, data={'body': botsay(body)})
        return res


async def add_labels(pid, iid, labels: Union[str, Iterable]):
    if isinstance(labels, string_types):
        labels = [labels]

    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')
        res = await gl.getitem(f"/projects/{pid}/merge_requests/{iid}")
        labels_ = set(res['labels'])
        labels_.update(labels)
        await gl.put(f"/projects/{pid}/merge_requests/{iid}", data={'id': pid, 'merge_request_iid': iid,
                                                                    'labels': ','.join(labels_)})


async def remove_labels(pid, iid, labels):
    if isinstance(labels, string_types):
        labels = [labels]

    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')
        res = await gl.getitem(f"/projects/{pid}/merge_requests/{iid}")
        labels_ = set(res['labels'])
        labels_.difference_update(labels)
        await gl.put(f"/projects/{pid}/merge_requests/{iid}", data={'id': pid, 'merge_request_iid': iid,
                                                                    'labels': ','.join(labels_)})


async def reset_labels(pid, iid):
    thelist = ["test-failed", "test-passed"]
    for release in SUPPORTED_TOOLCHAINS:
        thelist.extend([f'latest-{release}-failed', f'latest-{release}-passed'])
        thelist.extend([f'test-{release}-failed', f'test-{release}-passed'])
    await remove_labels(pid, iid, thelist)
    await add_labels(pid, iid, "test-needed")


async def approve(pid, iid, sha):
    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')
        logger.debug("get_label start")
        res = await gl.getitem(f"/projects/{pid}/merge_requests/{iid}")
        logger.debug("get_label end")
        for release in SUPPORTED_TOOLCHAINS:
            if f'latest-{release}-failed' in res['labels']:
                break
        else:
            await gl.post(f'/projects/{pid}/merge_requests/{iid}/approve', data={'id': pid, 'merge_request_iid': iid,
                                                                                 'sha': sha})
            return True

        return False


# Taken from jenkinsapi: build.block_until_complete
async def build_block_until_complete(build: Build, delay=25) -> None:
    logger.info("build_block_until_complete: %s", build.get_number())
    assert isinstance(delay, int)
    ugly = 1
    while build.is_running():
        if ugly % 5 == 0:
            logger.info(f"still running build {build.get_number()}")
        ugly += 1
        await asyncio.sleep(delay)

    logger.info(f"build done: {build.get_number()}")


# Taken from jenkinsapi: queue.block_until_building
async def block_until_building(qi: QueueItem, delay=25) -> Build:
    logger.info("block_until_building")
    logger.debug("block_until_building: %s", pformat(vars(qi)))
    while True:
        from jenkinsapi.custom_exceptions import NotBuiltYet
        from requests import HTTPError

        try:
            qi.poll()
            theBuild = qi.get_build()
            logger.debug(f"found the build: {pformat(vars(theBuild))}")
            return theBuild
        except (NotBuiltYet, HTTPError) as e:
            if '404 Client Error' in repr(e):
                logger.error(f"Queue no longer exists: {e!r}")
                return {"params": qi.get_parameters()}
            await asyncio.sleep(delay)
            continue
        except Exception as e:
            logger.error(f"block_until_building exception: {e!r}")
            await asyncio.sleep(delay)
            continue


# Taken from jenkinsapi: queue.block_until_complete
async def queue_block_until_complete(qi: QueueItem, delay=25, noteid=None, pid=None, iid=None) -> Build:
    logger.info("queue_block_until_complete")
    logger.debug("queue_block_until_complete: %s", pformat(vars(qi)))
    build = await block_until_building(qi, delay=25)
    if isinstance(build, dict):
        return build
    if noteid is not None:
        await update_comment("Build running: " + build.baseurl, pid, iid, noteid)

    await build_block_until_complete(build, delay)
    logger.info("queue_block_until_complete done")
    return build


def get_combinations(release: str) -> list:
    def build_combinations():
        res_ = []
        k = axes.keys()
        v = axes.values()

        combinations = itertools.product(*v)
        for combin in combinations:
            ress = {}
            for i_, kk in enumerate(k):
                ress[kk] = combin[i_]
            res_.append(ress)

        return res_

    job = MatrixJob(name="lcg_nightly_general_matrix", jenkins=j)
    filter_text = job.get_combinations_str()
    axes = job.get_axes()

    filter_expr = filter_text.replace('||', 'or').replace('&&', 'and')
    filter_expr = re.sub(rex_match, r"(re_search(\2, \1) is not None)", filter_expr)
    # expression-parser doesn't support string literals, so we need to extract them in advance
    constant_strings = set(re.findall("'(.*?)'", filter_expr))

    constants = {}
    for cs in constant_strings:
        i = len(constants)
        filter_expr = filter_expr.replace(f"'{cs}'", f"c_{i}")
        constants[f"c_{i}"] = cs

    matrix = []
    for c in build_combinations():
        # ignore builds for gcc8 until we drop them from all the stacks (devAdePT)
        if 'gcc8' in c['COMPILER']:
            continue

        comb = copy.copy(c)
        comb.update(constants)
        parser = expression.Expression_Parser(functions={'re_search': re.search},
                                              variables=comb,
                                              assignment=False)
        res = parser.parse(filter_expr)
        # print(res)
        if res:
            # comb_s = ",".join(f"{k}={v}" for k, v in c.items())
            # print(f"Combination: {comb_s} matches {s}")
            # matrix.append(comb)
            if comb['LCG_VERSION'] != release:
                continue
            comb_ = {'LCG_VERSION': release}
            for k in comb:
                if k not in ('HOST_ARCH', 'LABEL', 'COMPILER', 'BUILDTYPE'):
                    continue
                comb_[k] = comb[k]

            matrix.append(comb_)

    return matrix


async def run_build(job: MatrixJob, params: dict, project_id: Union[str, int], mr_id: int, config: Iterable[str],
                    release: str, build_type: str, **kwargs):
    payload = ["", "| Parameter | Value | ", "|:-----------|:-----------|"]
    builds = []
    target = params['TARGET']
    for comb in config:
        new_params = copy.copy(params)
        new_params.update(comb)
        pl_ = copy.copy(payload)
        for k, v in new_params.items():
            pl_.append(f" | {k} | {v} | ")

        if bot_dryrun:
            await post_comment('\n'.join(pl_), project_id, mr_id)
            builds.append(DryRunBuild(new_params))
        else:
            qi: QueueItem = job.invoke(build_params=new_params, cause="Merge request #" + str(mr_id), block=False)
            build = queue_block_until_complete(qi, noteid=None, pid=project_id, iid=mr_id)
            builds.append(build)

    if bot_dryrun:
        results = builds
    else:
        await post_comment(f'Queued {len(builds)} build(s) for {target} in {release}', project_id, mr_id)
        logger.info(f"Waiting for {len(builds)} to complete")
        results = await asyncio.gather(*builds)

    payload = ["", "### Release {0} for target {1}: @RESULT@".format(release, target), "",
               "| Build ID | Platform  | Result |", "|:-----------|:-----------|:-----------|"]

    build_passed = True
    logger.debug(f"Gathering results")
    for x in results:
        if isinstance(x, dict):
            # the queue expired before we could get the build information
            p = x.get("params", "")
            platform =  f"{p.get('LABEL')}-{p.get('COMPILER')}-{p.get('BUILDTYPE')}" if p else "Unknown Platform"
            payload.append(f" XXXXX | {platform} | {emoji_status['ERROR']}")
            continue
        x.poll()
        logger.debug(f"* {x.name} -> {x.get_status()} -> " + emoji_status.get(x.get_status(), u'?'))
        platform = x.name.split()[2].split('-', 1)[1]
        status = x.get_status()
        if 'ubuntu' not in platform and status != 'SUCCESS':
            build_passed = False

        payload.append(
            "[#{0}]({1}/consoleFull) | {2} | {3} ".format(x.get_number(), x.baseurl,
                                                          platform, emoji_status.get(status, u'?')))

    if release != '':
        if build_passed:
            await add_labels(project_id, mr_id, f"{build_type}-{release}-passed")
            payload[1] = payload[1].replace('@RESULT@', 'SUCCESS')
        else:
            await add_labels(project_id, mr_id, f"{build_type}-{release}-failed")
            payload[1] = payload[1].replace('@RESULT@', 'FAILED')

    body = '\n'.join(payload)

    if kwargs.get('reuse', False):
        # could still be None
        prev_id = await prev_results_note_id(project_id, mr_id, release)
    else:
        prev_id = None

    if prev_id:
        update_r, post_r, *_ = await asyncio.gather(
            update_comment(body, project_id, mr_id, prev_id),
            post_comment(f"""Repeated command `{kwargs.get('cmd_plaintext', "")}` for {release} done. Updated
                previous results: https://gitlab.cern.ch/sft/lcgcmake/-/merge_requests/{mr_id}#note_{prev_id}""",
                project_id, mr_id)
        )

        logger.info("Update comments result: ", update_r)
        logger.info("Post comment result: ", post_r)
        
    else:
        await post_comment(body, project_id, mr_id)


# noinspection PyUnusedLocal
async def latest_please(event: Event, gl: GitLabAPI):
    # global j
    # pid = event.project_id
    # iid = event.data['merge_request']['iid']
    # releases = ['dev3', 'dev3python3', 'dev4']
    #
    # if not event.data['user']['username'] in can_build:
    #     await post_comment(f"User {event.data['user']['username']} is not allowed to build!", pid, iid)
    #     return
    #
    # for rl in releases:
    #     # noinspection PyDictCreation
    #     params = {'VERSION_MAIN': event.data['merge_request']['source_branch'], 'LCG_TOOLCHAIN': rl,
    #               'DEPLOY_FAILING_BUILD': 'True'}
    #     asyncio.ensure_future(run_build(latest_job, params, pid, iid, rl, 'latest'))
    raise NotImplementedError()


# noinspection PyUnusedLocal
async def test_please(event: Event, gl: GitLabAPI, *args, **kwargs):
    async def usage(error=None):
        if error:
            await post_comment(
                f"Error: {error}\n\nUsage:\n```\nBot test (package) in ({'|'.join(SUPPORTED_TOOLCHAINS)}|all)+ [(also|only|) for [arch]-label-compiler-buildtype] please!\n```\nAnd Later\n```\nBot repeat please!\n```",
                pid, iid)

    global j
    pid = event.project_id
    iid = event.data['merge_request']['iid']
    try:
        source_url = event.data['merge_request']['source']['git_http_url']
    except KeyError:
        source_url = 'https://gitlab.cern.ch/sft/lcgcmake.git'

    if len(args) < 3:
        asyncio.ensure_future(usage("Not enough arguments!"))
        return

    targets = ""
    try:
        inIndex = args[:-1].index("in")
    except ValueError:
        asyncio.ensure_future(usage("Missing keyword 'in'!"))
        return
    else:
        targets = " ".join(args[:inIndex])
    
    if targets == "":
        asyncio.ensure_future(usage("No package specified!"))
        return

    after_in = args[inIndex+1:]

    releases = set()
    extra_platforms = []
    mode = 'DEV'
    for x in after_in:
        logger.debug(f'Processing word {x}, mode is {mode}')
        if x == 'also':
            logger.debug(f'Word is "also"')
            if mode == 'DEV' or mode == 'ALSO':
                logger.debug(f'Setting mode to "ALSO"')
                mode = 'ALSO'

            if mode == 'ONLY':
                logger.debug(f'Error: "also" in ONLY mode')
                asyncio.ensure_future(usage('Both <also> and <only> specified!'))
                return

            continue

        if x == 'only':
            logger.debug(f'Word is "only"')
            if mode == 'DEV' or mode == 'ONLY':
                logger.debug(f'Setting mode to "ONLY"')
                mode = 'ONLY'

            if mode == 'ALSO':
                logger.debug(f'Error: "only" in ALSO mode')
                asyncio.ensure_future(usage('Both <also> and <only> specified!'))
                return

            continue

        if x == 'for':
            logger.debug(f'Word is "for"')
            if mode == 'DEV':
                logger.debug(f'FOR is ONLY FOR')
                mode = 'ONLY'
            continue

        if x == 'all' and mode == 'DEV':
            logger.debug('Selecting ALL releases')
            releases = {'dev3', 'dev4'}
            continue

        if x in SUPPORTED_TOOLCHAINS and mode == 'DEV':
            logger.debug(f'Add release {x}')
            releases.add(x)
            continue

        if x not in SUPPORTED_TOOLCHAINS and mode == 'DEV':
            logger.debug(f'Assuming {x} is a toolchain')
            releases.add(x)
            continue

        if mode != 'DEV':
            logger.debug(f'Parsing platform {x}, mode is {mode}')
            parts = x.split('-')

            if len(parts) == 3:
                # in this case we don't have the host_arch
                host_arch = "x86_64"
                label = parts[0]
                compiler = parts[1]
                build_type = parts[2]

            elif len(parts) == 4:
                # in this case we have the host_arch
                host_arch = parts[0]
                label = parts[1]
                compiler = parts[2]
                build_type = parts[3]

            else:
                asyncio.ensure_future(usage(f'Platform too short or too long: {x}'))
                return

            if build_type not in ('Release', 'Debug', 'opt', 'dbg'):
                asyncio.ensure_future(usage(f'Invalid buildtype: {build_type}'))
                return

            build_type = "Release" if build_type in ("opt", "Release") else "Debug"

            for rl in releases:
                extra_platforms.append(
                    {'LCG_VERSION': rl,
                     'HOST_ARCH': host_arch,
                     'LABEL': label,
                     'COMPILER': compiler,
                     'BUILDTYPE': build_type,
                     })

    releases = list(releases)

    if not releases:
        print("Invalid release(s)")
        asyncio.ensure_future(usage())
        return

    submitted = False
    for rl in releases:
        params = {'BRANCH': event.data['merge_request']['source_branch'], 'LCG_VERSION': rl, 'TARGET': targets,
                    'LCGCMAKE_REPO': source_url}

        if mode == 'ALSO' or mode == 'DEV' and rl in SUPPORTED_TOOLCHAINS:
            combs = get_combinations(rl)
        else:
            combs = []

        combs.extend(extra_platforms)

        # Deduplicate. Taken from: https://stackoverflow.com/a/38521207/2652987
        combs = [dict(s) for s in set(frozenset(d.items()) for d in combs)]

        if combs:
            submitted = True
            asyncio.ensure_future(
                run_build(test_job, params, pid, iid, combs, rl, 'test', **kwargs, cmd_plaintext="test " + ' '.join(args))
            )
    if not submitted:
        asyncio.ensure_future(usage('No valid toolchain or platform combination found'))
        return



def tokenize_comment(message: str) -> (str, list[str]):
    """parses sentence like "bot test ... in ... please!" to tuple of
       verb (first word after 'bot') and list of following words excluding 'please!' """

    bott, msg = message.split(' ', 1)
    mesg, please = msg.rsplit(' ', 1)

    if not (bott.lower() == 'bot' and please.lower() == 'please!'):
        raise ValueError

    try:
        verb, args_s = mesg.split(' ', 1)
    except ValueError:
        verb = mesg
        args_s = ""

    args = args_s.split(' ')

    return (verb, args)


async def prev_results_note_id(project_id: Union[str, int], mr_id: Union[str, int], toolchain: str) -> Optional[str]:
    """returns id of last result overview matching `toolchain` (e.g. "dev3")
       `None` is returned if no previous results are found"""

    url = f"/projects/{project_id}/merge_requests/{mr_id}/notes"

    results_id = None

    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')

        async for comment in gl.getiter(url):
            try:
                if comment['author'] != 'lcgbot' and "Release" in comment['body'] and toolchain in comment['body']:
                    results_id = comment['id']
            except:
                continue

    if results_id:
        logger.info(f"Found previous results: {results_id}")

    return results_id


async def last_user_cmd(project_id: Union[str, int], mr_id: Union[str, int]) -> Optional[tuple[str, list[str]]]:
    """ returns verb and args of last command in merge request if found """

    url = f"/projects/{project_id}/merge_requests/{mr_id}/notes"

    latest_comment = None

    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')

        async for comment in gl.getiter(url):
            try:
                verb, args = tokenize_comment(comment['body'])
            except:
                continue

            if verb.lower().strip() in ['test', 'build']:
                latest_comment = (verb, args)

    if latest_comment:
        logger.info(f"found previous command: {latest_comment}")

    return latest_comment

# noinspection PyUnusedLocal
@bot.router.register("Note Hook", noteable_type="MergeRequest")
async def mr_comment_event(event: Event, gl: GitLabAPI, *args, **kwargs):
    logger.debug("mr_comment_event start")
    # print("args", args)
    # print("kwargs", kwargs)
    pid = event.project_id
    iid = event.data['merge_request']['iid']


    message = event.object_attributes['note'].strip()

    try:
        verb, args = tokenize_comment(message)
    except ValueError:
        return

    username = event.data['user']['username']
    logger.info("Username %r is giving command.", username)
    if username not in CAN_BUILD:
        await post_comment('Apologies, you do not seem to be authorised to give me commands.', pid, iid)
        return

    reuse = False

    if verb == 'repeat':
        logger.info(f"Asked to repeat command")
        verb, args = await last_user_cmd(pid, iid)
        logger.info(f"found command: {verb} {args}")
        reuse = True

    if verb == 'ping':
        # print(event.data)
        res = await post_comment('Pong?', pid, iid)
        note_id = res['id']
        await asyncio.sleep(10)
        await gl.put(f"/projects/{pid}/merge_requests/{iid}/notes/{note_id}", data={'body': botsay('Pong!')})
        # print('res', res)

    if verb == 'test' or verb == 'build':
        await test_please(event, gl, *args, reuse=reuse)

    logger.debug("mr_comment_event end")


# noinspection PyUnusedLocal
@bot.router.register("Merge Request Hook", action="open")
async def mr_open_event(event: Event, gl: GitLabAPI, *args, **kwargs):
    logger.debug("mr_open_event start")
    pid = event.project_id
    iid = event.data['object_attributes']['iid']
    # await gl.put(f"/projects/{pid}/merge_requests/{iid}", data={'body': {'labels': ''}})
    asyncio.ensure_future(add_labels(pid, iid, "test-needed"))
    logger.debug("mr_open_event end")


# noinspection PyUnusedLocal
@bot.router.register("Merge Request Hook", action="update")
async def mr_update_event(event: Event, gl: GitLabAPI, *args, **kwargs):
    logger.debug("mr_update_event start")
    if 'oldrev' in event.data['object_attributes']:
        pid = event.project_id
        iid = event.data['object_attributes']['iid']
        await reset_labels(pid, iid)

    logger.debug("mr_update_event end")


def init_jenkins(baseurl, username, password):
    global r, j
    # print "Setup SSO"
    r = SSORequester(baseurl=baseurl, username=username, password=password, ssl_verify=False)
    # print "Setup Jenkins API"
    j = Jenkins(baseurl=baseurl, requester=r)


if __name__ == "__main__":
    bot_debug = bool(os.environ.get('DEBUG', 0))
    bot_dryrun = bool(os.environ.get('DRYRUN', 0))

    setup_logging("bot.log", bot_debug)
    logger.info("Connecting to the new Jenkins...")
    try:
        init_jenkins(baseurl="https://lcgapp-services.cern.ch/spi-jenkins/", username=jenkins_username, password=jenkins_password)
    except Exception as e:
        logger.exception(f"Jenkins failed to initialise: {e!r}")

    if (r is None) or (j is None):
        exit(1)

    try:
        test_job = MatrixJob(name='lcg_mr_pipeline', jenkins=j)
        latest_job = test_job
    except Exception as e:
        logger.exception(f"Bot failed to start: {e!r}")
        exit(1)
    logger.info("Starting bot...")
    try:
        bot.run()
    except Exception as e:
        logger.exception(f"Bot failed with exception: {e!r}")
        exit(1)
