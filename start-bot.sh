#!/bin/bash
#TAG=latest
#TAG=canary
#TAG=canary
#TAG=aarch
#TAG=multitest
#TAG=newsso
TAG=cookies5
docker login gitlab-registry.cern.ch

[ -e bot.cid ] && (docker container stop `cat bot.cid` 2>/dev/null; docker container rm `cat bot.cid` 2>/dev/null; rm bot.cid)
if [ $# -eq 0 ]; then
docker run --name bot --net=host --pull=always -p8080:8080 -d --cidfile bot.cid --restart=always gitlab-registry.cern.ch/sft/lcgbot:$TAG
else
docker run --name bot --net=host --pull=always -p8080:8080 -e DEBUG=1 -e DRYRUN=1  -it --rm gitlab-registry.cern.ch/sft/lcgbot:$TAG
fi

shred ~/.docker/config.json
rm ~/.docker/config.json

